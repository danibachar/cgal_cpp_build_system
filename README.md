# CG_3D_IDC
Computational Geometry For 3D Printer course automation testing for Mac and Ubuntu 16

# Setup #
Scripts (python, make, cmake, sh) were tested on mac OS Mojave.


### Mac OS###
* Make sure brew is installed -> `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
* Or just go to Homebrew website `https://brew.sh/`
* Run `sudo ./Setup/mc_os.sh` to install Project dependencies

* On mac there is an option to run projects in Xcode (:
  Take a look at `knn` sample project (a hint check out the `Linked Frameworks and Libraries` section on how to add the relevant Libraries)


### Ubuntu 16.04###
* run Run `sudo ./Setup/ubuntu_16.sh` to install Project dependencies


## Project Description ##

* Needs Python 3 and above to work
* ### Python Tests:

In the PytohnTest folder there is an example build system for a sample cpp project, with running some python driven tests
To run it just `cd PythonTest/` into the folder
Run `python build_run_test.py`

`BaseFiles` folder holds the base files which we duplicated the folder to give an automated example of testing users projects/assignments

In each folder tested there is a `logs` directory which includes the script stages logs (cmake, make, and the executable - output of the code you wrote in cpp : )



### General:

In the base of the folder there is a python script named `build_run_test.py`

This Script needs to be update before every assignment - TODO - improve automation in the future

The HW_* folder needs to include sub-folder named STUDENTS, and in this folder open a directory with the student's id and files

Update the input/output files names, and the executable named
and run









### References about project dependencies
#### CMAKE VERSION 2.8.11
* https://askubuntu.com/questions/610291/how-to-install-cmake-3-2-on-ubuntu
* https://stackoverflow.com/questions/32185079/installing-cmake-with-home-brew

#### CGAL

#### GMP

#### MPFR

### BOOST

### GCC
 Verify - `gdb --version`
