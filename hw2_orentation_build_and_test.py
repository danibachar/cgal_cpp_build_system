# -*- coding: utf-8 -*-
import os
import sys
p = os.getcwd()
sys.path.insert(0, p)
from tools import Builder, Tester, Differentiator, safe_copy, safe_open
####################################################################################################
#
# Full running example of the script
#
# python hw1_build_and_test.py /Users/danielbachar/Documents/IDC/CG_3D_IDC/HW_1/basefiles/CMakeLists.txt /Users/danielbachar/Documents/IDC/CG_3D_IDC/HW_1/STUDENTS /Users/danielbachar/Documents/IDC/CG_3D_IDC/HW_1/TESTS
####################################################################################################

########################################################################################
# This script takes 2 arguments
# 1) absulute system path for the CMakeLists.txt file with the cmake configuration
# 2) absulute system path for the STUDENTS directory which in each directory will
#	 have the relevant .cpp/.hpp/.h files
# 3) absulute system path for the TEST directory which in each directory will
#    have the relevant input files for the executible as described in the assignemnt description
#
#
# * The script then will copy the  base cmake file into the folder_name
# * The script then will run cmake and make to build the executible
# * The script will generate log files within the student folder
#   For the cmake and the make commands
#
# Example that the script will run in case of
# python hw1_build_and_test.py
#            arg2           '/Users/danielbachar/Documents/IDC/CG_3D_IDC/HW_1/basefiles/CMakeLists.txt'
#            arg3           '/Users/danielbachar/Documents/IDC/CG_3D_IDC/HW_1/STUDENTS
#            arg4           '/Users/danielbachar/Documents/IDC/CG_3D_IDC/HW_1/TESTS'
#
# Builder.full_build(
# '/Users/danielbachar/Documents/IDC/CG_3D_IDC/HW_1/basefiles/CMakeLists.txt',
# '/Users/danielbachar/Documents/IDC/CG_3D_IDC/HW_1/STUDENTS'
# )
#
########################################################################################

num_of_args = len(sys.argv)
if num_of_args != 4:
	print('build.py `absulute path for CMakeLists.txt` `absulute path for the base folder of students`')
	exit(1)

cmake_file_path = sys.argv[1]
students_base_folder = sys.argv[2]
tests_base_folder = sys.argv[3]

students_folder_list = list(
	filter(
		lambda x: not x.startswith('.'), list(os.listdir(students_base_folder))
	)
)

tests_folder_list = list(
	filter(
		lambda x: not x.startswith('.'), list(os.listdir(tests_base_folder))
	)
)

# Buidling
for student in students_folder_list:
	full_student_folder = '/'.join([students_base_folder, student])
	Builder.build(student, students_base_folder, cmake_file_path)
	print('DONE BUILDING FOR = {}'.format(student))

TEST_ARG_MAP = {
	"1": ['8', 'polygon.txt'],
	"2": ['5', 'polygon.txt'],
	"3": ['4', 'polygon.txt'],
	"4": ['3', 'polygon.txt'],
	"5": ['2', 'polygon.txt'],
	"6": ['1', 'polygon.txt'],
	"7": ['100', 'polygon.txt'],
	"8": ['36', 'polygon.txt'],
	"9": ['1', 'polygon.txt'],
	"10": ['2.25', 'polygon.txt'],
	"11": ['4', 'polygon.txt'],
	"12": ['6740.41', 'polygon.txt'],
	"13": ['6889', 'polygon.txt'],
	"14": ['7056', 'polygon.txt'],
	"15": ['1', 'polygon.txt'],
	"16": ['1', 'polygon.txt'],



}

diff_tool = Differentiator()
tests_results = {}
exec_file_name = "print_orientation_2"
for test in tests_folder_list:
	for student in students_folder_list:
		args = TEST_ARG_MAP.get(str(test),[])

		full_student_folder = Tester.preprocess(
			tests_base_folder=tests_base_folder,
			test_name=test,
			arguments=args,
			students_base_folder=students_base_folder,
			student=student
		)

		Tester._test(full_student_folder, exec_file_name, list(args), test)

		test_original_results = '/'.join([tests_base_folder, test, 'results.txt'])
		test_results_to_check = '/'.join([full_student_folder, 'logs', test+'_'+exec_file_name+'.log'])

		html_results = diff_tool.compare_html(
			test_original_results, test_results_to_check
		)
		html_file_name = '/'.join(
			[os.path.dirname(os.path.dirname(tests_base_folder)), 'HW_2_orientation', 'results', student, str(test)]
		)+'.html'

		with safe_open(html_file_name, 'w') as f:
			f.write(html_results)
		print('DONE TEST - {} FOR {}'.format(test, student))
