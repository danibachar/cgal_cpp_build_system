brew install gcc48
brew install cgal
brew install boost
brew install gmp
brew install mpfr
brew install cmake

export PATH="/usr/local/bin:$PATH"
export PATH=/usr/local/Cellar/gcc:$PATH
