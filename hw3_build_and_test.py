# -*- coding: utf-8 -*-
import os
import sys
import time
p = os.getcwd()
sys.path.insert(0, p)
from tools import Builder, Tester, Differentiator, safe_copy, safe_open
####################################################################################################
#
# Full running example of the script
#
# python hw1_build_and_test.py /Users/danielbachar/Documents/IDC/CG_3D_IDC/HW_1/basefiles/CMakeLists.txt /Users/danielbachar/Documents/IDC/CG_3D_IDC/HW_1/STUDENTS /Users/danielbachar/Documents/IDC/CG_3D_IDC/HW_1/TESTS
####################################################################################################

########################################################################################
# This script takes 2 arguments
# 1) absulute system path for the CMakeLists.txt file with the cmake configuration
# 2) absulute system path for the STUDENTS directory which in each directory will
#	 have the relevant .cpp/.hpp/.h files
# 3) absulute system path for the TEST directory which in each directory will
#    have the relevant input files for the executible as described in the assignemnt description
#
#
# * The script then will copy the  base cmake file into the folder_name
# * The script then will run cmake and make to build the executible
# * The script will generate log files within the student folder
#   For the cmake and the make commands
#
# Example that the script will run in case of
# python hw1_build_and_test.py
#            arg2           '/Users/danielbachar/Documents/IDC/CG_3D_IDC/HW_1/basefiles/CMakeLists.txt'
#            arg3           '/Users/danielbachar/Documents/IDC/CG_3D_IDC/HW_1/STUDENTS
#            arg4           '/Users/danielbachar/Documents/IDC/CG_3D_IDC/HW_1/TESTS'
#
# Builder.full_build(
# '/Users/danielbachar/Documents/IDC/CG_3D_IDC/HW_1/basefiles/CMakeLists.txt',
# '/Users/danielbachar/Documents/IDC/CG_3D_IDC/HW_1/STUDENTS'
# )
#
########################################################################################

num_of_args = len(sys.argv)
if num_of_args != 4:
	print('build.py `absulute path for CMakeLists.txt` `absulute path for the base folder of students`')
	exit(1)

cmake_file_path = sys.argv[1]
students_base_folder = sys.argv[2]
tests_base_folder = sys.argv[3]

students_folder_list = list(
	filter(
		lambda x: not x.startswith('.'), list(os.listdir(students_base_folder))
	)
)

tests_folder_list = list(
	filter(
		lambda x: not x.startswith('.'), list(os.listdir(tests_base_folder))
	)
)
tests_folder_list.sort(key=lambda x: int(x))
# Buidling
# for student in students_folder_list:
# 	full_student_folder = '/'.join([students_base_folder, student])
# 	Builder.build(student, students_base_folder, cmake_file_path)
# 	print('DONE BUILDING FOR = {}'.format(student))

TEST_ARG_MAP = {
	"1": ['1', 'points_grid_1.txt'],
	"2": ['1', 'points_grid_9.txt'],
	"3": ['0.25', 'points_grid_9.txt'],
	"4": ['9', 'points_grid_9.txt'],
	"5": ['1', 'points_grid_16.txt'],
	"6": ['16', 'points_grid_16.txt'],
	"7": ['1', 'points_grid_144.txt'],
	"8": ['144', 'points_grid_144.txt'],
	"9": ['1', 'points_grid_400.txt'],
	"10": ['400', 'points_grid_400.txt'],
	"11": ['1', 'points_grid_961.txt'],
	"12": ['961', 'points_grid_961.txt'],

	"13": ['1', 'points_circle_3.txt'],
	"14": ['1', 'points_circle_4.txt'],
	"15": ['1', 'points_circle_8.txt'],
	"16": ['1', 'points_circle_16.txt'],
	"17": ['1', 'points_circle_32.txt'],
	"18": ['1', 'points_circle_64.txt'],
	"19": ['1', 'points_circle_128.txt'],
	"20": ['1', 'points_circle_256.txt'],
	"21": ['1', 'points_circle_512.txt'],
	"22": ['1', 'points_circle_1024.txt'],

	"23": ['1', 'points_circle_ap_6.txt'],
	"24": ['1', 'points_circle_ap_8.txt'],
	"25": ['1', 'points_circle_ap_16.txt'],
	"26": ['1', 'points_circle_ap_32.txt'],
	"27": ['1', 'points_circle_ap_64.txt'],
	"28": ['1', 'points_circle_ap_128.txt'],
	"29": ['1', 'points_circle_ap_256.txt'],
	"30": ['1', 'points_circle_ap_512.txt'],
	"31": ['1', 'points_circle_ap_1024.txt'],
	"32": ['1', 'points_circle_ap_2048.txt'],

	"33": ['1', 'points_mix_63.txt'],
	"34": ['2', 'points_mix_63.txt'],
	"35": ['4', 'points_mix_63.txt'],
	"36": ['6', 'points_mix_63.txt'],
	"37": ['9', 'points_mix_63.txt']

}

import csv
csv_file_name = "/Users/danielbachar/Documents/IDC/cgal_cpp_build_system/HW3/results/table.csv"
exec_file_name = "max_cover"
start = time.time()

with safe_open(csv_file_name, 'w') as f:  # Just use 'w' mode in 3.x

	#Preparing CSV
	w = csv.DictWriter(f, ['execution'] + students_folder_list)
	w.writeheader()

	for test in ['10']:

		tests_results = {}
		args = list(TEST_ARG_MAP.get(str(test),[]))
		tests_results["execution"] = '{}'.format(args)

		for student in students_folder_list:

			full_student_folder = Tester.preprocess(
				tests_base_folder=tests_base_folder,
				test_name=test,
				arguments=args,
				students_base_folder=students_base_folder,
				student=student
			)

			Tester._test(full_student_folder, exec_file_name, args, test)

			test_results_to_check = '/'.join([full_student_folder, 'logs', test+'_'+exec_file_name+'.log'])

			lines = [line.rstrip('\n') for line in open(test_results_to_check)]
			tests_results[student] = '\n'.join(lines)

		w.writerow(tests_results)
		print('DONE TEST - {}'.format(test))

end = time.time()
print('Total testing run time - {}'.format(end - start))
