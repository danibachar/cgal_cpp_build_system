import os
import subprocess
import random
from tools.utils import safe_open


ARGUMENTS = ['polygon.txt', 'queries.txt']

def rundom_dots_in_range(filename:str, min_in_range:int, max_in_range:int, num_of_dots:int=random.randint(8,20)):

    file_stream = safe_open(filename, "wb+")
    t = str("{}\n".format(num_of_dots)).encode()
    print(t)
    file_stream.write(t)

    for line_number in range(num_of_dots):
        left = random.randint(min_in_range, max_in_range)
        right = random.randint(min_in_range, max_in_range)

        if left > right:
            tmp = left
            left =  right
            right = tmp
        file_stream.write(str("{} {}\n".format(left, right)).encode())
    file_stream.close()

folders = [ name for name in os.listdir() if os.path.isdir(os.path.join(name)) ]

for folder in folders:
    # Generate tests
    for (idx,filename) in enumerate(ARGUMENTS):
        fullname = '/'.join([folder, filename])
        print(fullname)
        if idx == 0:
            # Generate polygon
            rundom_dots_in_range(fullname, -1, 1)

        if idx == 1:
            # Generate query file
            num = random.randint(20,40)
            rundom_dots_in_range(fullname, -2, 2, num_of_dots=num)
