import os
import sys
# Hack to import from local folder (utils)
p = os.getcwd()
sys.path.insert(0, p)

from tools.build import Builder
from tools.test import Tester
from tools.diff import Differentiator
from tools.polygon_generator import PolygonGenerator
from tools.utils import safe_copy, safe_open
