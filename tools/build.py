# -*- coding: utf-8 -*-
import os
import sys
import shutil

from tools.utils import run_cmake, run_make, safe_copy

class Builder(object):


	"""docstring for Tester."""
	def __init__(self, arg):
		super(Builder, self).__init__()
		self.arg = arg
		self.cache = Cache('/tmp/mycachedir')

	@staticmethod
	def _build(full_student_folder:str, log_prefix:str = None):
		try:
			run_cmake(full_student_folder, log_prefix)
		except Exception as e:
			print('CMAKE Error - {}'.format(e))
		try:
			run_make(full_student_folder, log_prefix)
		except Exception as e:
			print('MAKE Error = {}'.format(e))

	@staticmethod
	def full_build(cmake_file_absulut_path:str, student_base_folder_absulut_path:str):
		students_folder_list = list(
			filter(
				lambda x: not x.startswith('.'), list(os.listdir(student_base_folder_absulut_path))
			)
		)
		for student in students_folder_list:
			Builder.build(student, student_base_folder_absulut_path, cmake_file_absulut_path)

	@staticmethod
	def build(student:str, student_base_folder_absulut_path:str, cmake_file_absulut_path:str):
		full_student_folder = '/'.join([student_base_folder_absulut_path, student])
		safe_copy(cmake_file_absulut_path, full_student_folder)
		Builder._build(full_student_folder)
