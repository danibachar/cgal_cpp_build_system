# -*- coding: utf-8 -*-

import os
import sys
import difflib
from tools.utils import safe_open

class Differentiator(object):

    """docstring for Tester."""
    def __init__(self, arg=None):
        super(Differentiator, self).__init__()
        self.arg = arg
        self.html_diff = difflib.HtmlDiff()

    def compare(self, source_file:str, tested_file:str):
        origin = safe_open(source_file, 'r')
        tested = safe_open(tested_file, 'r')
        diff = difflib.ndiff(origin.readlines(), tested.readlines())
        changes = [l for l in diff if l.startswith('+ ') or l.startswith('- ')]
        return changes

    def compare_html(self, source_file:str, tested_file:str):
        origin = safe_open(source_file, 'r')
        tested = safe_open(tested_file, 'r')
        html_diff_file = self.html_diff.make_file(origin.readlines(), tested.readlines())
        return html_diff_file
