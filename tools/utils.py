import os
import subprocess
import sys
import shutil
from threading import Timer

# Consts
CMAKE_LOG_SUFFIX = 'cmake'
MAKE_LOG_SUFFIX = 'make'
EXEC_LOG_SUFFIX = 'exec'

SUPORTED_ARGS_FILES = set(['.txt', '.stl'])

def is_supported_argument_file(arg:str):
	try:
		ext = os.path.splitext(arg)[-1].lower()
		return (ext in SUPORTED_ARGS_FILES)
	except Exception as e:
		print('Error in checking argument support - {}'.foramt(e))
		return False

def safe_open(file_name_with_dierctory:str, permision="wb+"):
	if not os.path.exists(os.path.dirname(file_name_with_dierctory)):
	    try:
	        os.makedirs(os.path.dirname(file_name_with_dierctory))
	    except OSError as exc: # Guard against race condition
	        if exc.errno != errno.EEXIST:
	            raise

	return open(file_name_with_dierctory, permision)

# GENERAL
def _run_sheel_command(in_directory:str, log_name_suffix:str, command:list, log_level:int = 1):
	log_name = '/'.join([in_directory, 'logs', log_name_suffix])
	log_stream = safe_open(log_name, "wb+")
	if log_level > 1:
		log_stream.write('runnning commands = {} in cwd = {}\n'.format(command,in_directory ).encode())

	p = subprocess.Popen(command, cwd=in_directory, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	timer = Timer(10, p.kill)
	timer.start()
	if log_level > 0:
		for line in iter(p.stdout.readline, b''):
			log_stream.write(line)
		for line in iter(p.stderr.readline, b''):
			log_stream.write(line)
	timer.cancel()
	# p.wait()
	log_stream.close()

# arguments:str are usualy a file or 2 to add as args for the executible
def run_executible(folder_name:str, exec_file_name:str, arguments:str, log_prefix:str=None):
	commands = ['./'+exec_file_name] + arguments
	_run_sheel_command(folder_name, _generate_log_name(exec_file_name, log_prefix), commands)

# CMAKE
def run_cmake(folder_name:str, log_prefix:str=None):
	_run_sheel_command(folder_name, _generate_log_name(CMAKE_LOG_SUFFIX, log_prefix), ["cmake", "."])

# MAKE
def run_make(folder_name:str, log_file_name:str, log_prefix:str=None):
	_run_sheel_command(folder_name, _generate_log_name(MAKE_LOG_SUFFIX,log_prefix), ["make"])

def _generate_log_name(suffix,prefix=None):
	if prefix is None:
		prefix = ''
	return '_'.join([prefix,suffix])+'.log'

def safe_copy(src_file:str, dest:str):
	try:
		shutil.copy(src_file, dest)
	except Exception as e:
		print('COPY Error = {}'.format(e))
