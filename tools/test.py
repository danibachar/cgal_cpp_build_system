# -*- coding: utf-8 -*-
import shutil
from tools.utils import run_executible, safe_copy, is_supported_argument_file
from tools.diff import Differentiator

class Tester(object):

    """docstring for Tester."""
    def __init__(self, arg):
        super(Tester, self).__init__()
        self.arg = arg

    @staticmethod
    def _test(folder_with_exec_to_test:str, exec_name:str, arguments, log_prefix:str = None):
        try:
            run_executible(folder_with_exec_to_test, exec_name, arguments, log_prefix)
        except Exception as e:
	           print('EXEC Error = {}'.format(e))

    @staticmethod
    def preprocess(
        tests_base_folder:str,
        test_name:str,
        arguments:list,
        students_base_folder:str,
        student:str
    ):
        full_student_folder = '/'.join([students_base_folder, student])

        # Copying only files - sanityze
        for arg in arguments:
            if is_supported_argument_file(arg):
                argument_src_file_full_path = '/'.join([tests_base_folder, test_name, arg])

                safe_copy(argument_src_file_full_path, full_student_folder)

        return full_student_folder
