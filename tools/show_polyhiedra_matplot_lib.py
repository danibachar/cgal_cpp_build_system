import numpy as np
import pyny3d.geoms as pyny
import matplotlib.pyplot as plt
import matplotlib.path as mplPath
#for plot
import shapely.geometry as shapely

class MyPoly(shapely.Polygon):
    def __init__(self,points):
        super(MyPoly,self).__init__(points)
        self.points = points
        self.points_shapely = [shapely.Point(p[0],p[1]) for p in points]

def convert_to_shapely_points_and_poly(poly,points):
    poly_shapely = MyPoly(poly)
    points_shapely = [shapely.Point(p[0],p[1]) for p in points]
    return poly_shapely,points_shapely

def plot(poly_init,points_init):
    #convert to shapely poly and points
    poly,points = convert_to_shapely_points_and_poly(poly_init,points_init)

    #plot polygon
    plt.plot(*zip(*poly.points))

    #plot points
    xs,ys,cs = [],[],[]
    for point in points:
        xs.append(point.x)
        ys.append(point.y)
        color = inPoly(poly,point)
        cs.append(color)

    plt.scatter(xs,ys, c = cs , s = 20*4*2)

    #setting limits
    axes = plt.gca()
    axes.set_xlim([min(xs)-5,max(xs)+50])
    axes.set_ylim([min(ys)-5,max(ys)+10])

    plt.show()


def isBetween(a, b, c): #is c between a and b ?
    crossproduct = (c.y - a.y) * (b.x - a.x) - (c.x - a.x) * (b.y - a.y)
    if abs(crossproduct) > 0.01 : return False   # (or != 0 if using integers)

    dotproduct = (c.x - a.x) * (b.x - a.x) + (c.y - a.y)*(b.y - a.y)
    if dotproduct < 0 : return False

    squaredlengthba = (b.x - a.x)*(b.x - a.x) + (b.y - a.y)*(b.y - a.y)
    if dotproduct > squaredlengthba: return False

    return True

def get_edges(poly):
    # get edges
    edges = []
    for i in range(len(poly.points)-1):
        t = [poly.points_shapely[i],poly.points_shapely[i+1]]
        edges.append(t)
    return edges


def inPoly(poly,point):
    if poly.contains(point) == True:
        return 1
    else:
        for e in get_edges(poly):
            if isBetween(e[0],e[1],point):
                return 1
    return 0

poly1 = pyny.Polygon(np.array([[0.479985, 0.399706]
,[0.429292, 0.412958]
,[0.193092, 0.47019]
,[-0.0136558, 0.482298]
,[-0.0134248, 0.294979]
,[-0.0726033, 0.136449]
,[-0.119317, -0.0257714]
,[-0.218883, -0.00181662]
,[-0.275114, -0.0213098]
,[-0.256297, -0.00198302]
,[-0.164621, 0.15393]
,[-0.110004, 0.0905387]
,[-0.314211, 0.32845]
,[-0.241565, 0.276547]
,[-0.131882, 0.193471]
,[-0.251243, 0.315806]
,[-0.195788, 0.396407]
,[-0.224421, 0.481199]
,[-0.0308258, 0.417573]
,[-0.177552, 0.492275]
,[-0.453276, 0.457052]
,[-0.391311, 0.459394]
,[-0.470595, 0.347151]
,[-0.354753, -0.135054]
,[-0.400588, -0.262989]
,[-0.413333, -0.45148]
,[-0.316419, -0.499887]
,[-0.0325005, -0.292363]
,[0.159219, -0.103793]
,[0.216178, -0.0731515]
,[0.336742, -0.304598]
,[0.0664749, -0.281079]
,[-0.0492653, -0.420069]
,[0.165132, -0.432095]
,[0.433724, -0.328]
,[0.394135, -0.17759]
,[0.478125, -0.324456]
,[0.46708, -0.205822]
,[0.41751 ,-0.182868]
,[0.473507 ,-0.080633]
,[0.490821 ,-0.0289114]
,[0.456846 ,-0.0788427]
,[0.462558, -0.0132637]
,[0.479628, 0.231482]
,[0.442079, 0.123571]
,[0.421013, 0.102661]
,[0.387675, 0.0788976]
,[0.312218 ,0.0939022]
,[0.263118,0.100847]
,[0.195505,0.183886]]))

poly = {1:[[0.479985, 0.399706]
,[0.429292, 0.412958]
,[0.193092, 0.47019]
,[-0.0136558, 0.482298]
,[-0.0134248, 0.294979]
,[-0.0726033, 0.136449]
,[-0.119317, -0.0257714]
,[-0.218883, -0.00181662]
,[-0.275114, -0.0213098]
,[-0.256297, -0.00198302]
,[-0.164621, 0.15393]
,[-0.110004, 0.0905387]
,[-0.314211, 0.32845]
,[-0.241565, 0.276547]
,[-0.131882, 0.193471]
,[-0.251243, 0.315806]
,[-0.195788, 0.396407]
,[-0.224421, 0.481199]
,[-0.0308258, 0.417573]
,[-0.177552, 0.492275]
,[-0.453276, 0.457052]
,[-0.391311, 0.459394]
,[-0.470595, 0.347151]
,[-0.354753, -0.135054]
,[-0.400588, -0.262989]
,[-0.413333, -0.45148]
,[-0.316419, -0.499887]
,[-0.0325005, -0.292363]
,[0.159219, -0.103793]
,[0.216178, -0.0731515]
,[0.336742, -0.304598]
,[0.0664749, -0.281079]
,[-0.0492653, -0.420069]
,[0.165132, -0.432095]
,[0.433724, -0.328]
,[0.394135, -0.17759]
,[0.478125, -0.324456]
,[0.46708, -0.205822]
,[0.41751 ,-0.182868]
,[0.473507 ,-0.080633]
,[0.490821 ,-0.0289114]
,[0.456846 ,-0.0788427]
,[0.462558, -0.0132637]
,[0.479628, 0.231482]
,[0.442079, 0.123571]
,[0.421013, 0.102661]
,[0.387675, 0.0788976]
,[0.312218 ,0.0939022]
,[0.263118,0.100847]
,[0.195505,0.183886]]}
points = {1:[(0.195505,0.183886),(0.263118,0.100847)]}
#poly2 = pyny.Polygon(np.array([[0, 0, 3], [0.5, 0, 3], [0.5, 0.5, 3], [0, 0.5, 3]]))
# polyhedron = pyny.Polyhedron.by_two_polygons(poly1, poly2)
# polyhedron.plot('b')
# poly1.plot('b')
# plt.show()
#print bbPath.contains_points(points) #0 if outside, 1 if inside
for data in zip(iter(poly.values()),iter(points.values())):
    plot(data[0],data[1])
