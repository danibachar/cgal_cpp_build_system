import os
import subprocess

# Consts
CMAKE_FILE_NAME = "CMakeLists.txt"
CMAKE_LOG_SUFFIX = 'cmake.log'
MAKE_LOG_SUFFIX = 'make.log'
EXEC_LOG_SUFFIX = 'executable.log'

EXCEPT_DIR_SET = set(['basefiles'])

# CMAKE
def run_cmake(folder_name:str):
	run_sheel_command(folder_name, CMAKE_LOG_SUFFIX, ["cmake", "."])

# MAKE
def run_make(folder_name:str):
	run_sheel_command(folder_name, MAKE_LOG_SUFFIX, ["make"])

# RUN
def run(folder_name:str):
	run_sheel_command(folder_name, EXEC_LOG_SUFFIX,['./executable', '2', '10_points_3d.txt', '10_points_3d.txt'])

def safe_open(file_name_with_dierctory:str, permision="wb+"):
	if not os.path.exists(os.path.dirname(file_name_with_dierctory)):
	    try:
	        os.makedirs(os.path.dirname(file_name_with_dierctory))
	    except OSError as exc: # Guard against race condition
	        if exc.errno != errno.EEXIST:
	            raise

	return open(file_name_with_dierctory, permision)


# GENERAL
def run_sheel_command(in_directory:str, log_name_suffix:str, command:list):
	log_name = '/'.join([in_directory, 'logs', log_name_suffix])
	# my_pre_open(log_name)
	log_stream = safe_open(log_name, "wb+")

	p = subprocess.Popen(command, cwd=in_directory, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	for line in iter(p.stdout.readline, b''):
		log_stream.write(line)
	for line in iter(p.stderr.readline, b''):
		log_stream.write(line)
	p.wait()
	log_stream.close()


for s_id in os.listdir():
	if s_id.lower() in EXCEPT_DIR_SET:
		continue
	if os.path.isdir(s_id):
		print('checking... - {}'.format(s_id))
		dir_files = os.listdir(s_id)
		if CMAKE_FILE_NAME not in dir_files:
			raise

		try:
			run_cmake(s_id)
		except Exception as e:
			print('CMake error = {}'.format(e))

		try:
			run_make(s_id)
		except Exception as e:
			print('Make error = {}'.format(e))

		try:
			run(s_id)
		except Exception as e:
			print('Exec error = {}'.format(e))
